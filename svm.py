from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import numpy as np
from scipy._lib.six import xrange

import Data as ds
from pandas_ml import ConfusionMatrix

POS_DIR = "..\\images\\pos"
NEG_DIR = "..\\images\\neg"

# SVM model is deprecated-- https://www.tensorflow.org/api_docs/python/tf/contrib/learn/SVM
# See https://github.com/tensorflow/tensorflow/blob/r1.9/tensorflow/contrib/learn/README.md


# Using custom implementation as found here:
# https://www.kaggle.com/stansilas/non-linear-svm-in-tensorflow
# https://github.com/eakbas/tf-svm

flowers = ds.Data(POS_DIR, NEG_DIR, 0.8)

train_data, train_labels = flowers.get_training_data()
test_data, test_labels = flowers.get_testing_data()

TRAIN_SIZE, NUM_FEATURES = train_data.shape
BATCH_SIZE = 100
TEST_SIZE = len(test_labels)

# convert labels to +1,-1
train_labels[train_labels == 0] = -1
test_labels[test_labels == 0] = -1

x = tf.placeholder("float", shape=[None, NUM_FEATURES])
y = tf.placeholder("float", shape=[None, 1])

W = tf.Variable(tf.zeros([NUM_FEATURES, 1]))
b = tf.Variable(tf.zeros([1]))
y_raw = tf.matmul(x, W) + b

regularization_loss = 0.5*tf.reduce_sum(tf.square(W))
hinge_loss = tf.reduce_sum(tf.maximum(tf.zeros([BATCH_SIZE, 1]), 1 - y*y_raw))
svm_loss = regularization_loss + 5*hinge_loss
train_step = tf.train.GradientDescentOptimizer(0.01).minimize(svm_loss)

predicted_class = tf.sign(y_raw)
correct_prediction = tf.equal(y, predicted_class)
accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))

with tf.Session() as sess:
    init = tf.global_variables_initializer()
    sess.run(init)

    for step in xrange(len(train_data) // BATCH_SIZE):

        offset = (step * BATCH_SIZE) % len(train_data)
        batch_data = train_data[offset:(offset + BATCH_SIZE), :]
        batch_labels = train_labels[offset:(offset + BATCH_SIZE)]

        train_step.run(feed_dict={x: batch_data, y: batch_labels})
        svm_loss.eval(feed_dict={x: batch_data, y: batch_labels})

    predictions = sess.run(predicted_class, feed_dict={x: test_data, y: np.empty([TEST_SIZE, 1])})
    actual = test_labels

    actual[actual == -1] = 0
    predictions[predictions == -1] = 0

    cm = ConfusionMatrix(actual.flatten().astype(int), predictions.flatten().astype(int))

    print("Summary:")
    cm.print_stats()
    print("Confusion Matrix:")
    print(cm)

