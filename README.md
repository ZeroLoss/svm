# Support Vector Machine (SVM) Kiwifruit Classifier

### Classification Description
  We will be training a linear SVM to detect kiwifruit flowers. An SVM attempts to find the optimal hyperplane (i.e. the line which best separates the two classes of points). New points can then be quickly classified, by checking which side of the hyperplane they fall.
  
  We train on small regions of the image, annotated as either 'flower' or 'not flower'. On these annotations, our model is able to achieve around 93% classification accuracy. A number of pre-computed datasets and corresponding model weights are included. We are unable to train directly on the raw image regions- we must extract a vector of 'features' that our SVM can handle. 
  
  Using these model weights, we search new images for regions that our model classifies as a flower.
  
-----------------------

### Minimum Requirements
#### Hardware
No minimum requirement, though better hardware will greatly reduce the runtime.
#### Software
- Operating System: Either Linux or Windows
- Weka 3.8. This is licenced under the GNU General Public Licence and can be downloaded [here](https://www.cs.waikato.ac.nz/ml/weka/downloading.html).
    * We will also need an SVM package. libLINEAR or libSVM should work fine, and can be installed via the Weka package manager.
    
- Python 3.6 with the following packages:
    * opencv
    * skimage
    * liac-arff
    * numpy

-----------------------

### Best Performance Recommendation
To achieve best results when detecting flower regions in a larger image, try to minimize the false positive rate. The false negative rate is less important, as many region variations are fed into the classifier, meaning flowers should still be detected.

In terms of runtime, the whole process is currently single-threaded. This means the process is heavily CPU bound- the CPU frequency will be the main factor in the runtime.

-----------------------

### Preparation Of The Data
  The python scripts should handle all of the main image formats (.jpg, .png).

  The dataset files used for training / testing are structured as follows:
  
- Each line defines regions from a new file. Each line contains a number of parameters, separated by spaces.
- The first parameter specifies the path to the image file. This can be relative or absolute.
- The next parameter specifies the number of regions extracted from this image.
- The remaining parameters are in groups of four, specifying the x and y value of the top left corner, as well as the width and height of the region.

  Four of these datasets are required. The positive and negative regions must be defined for both the training and testing sets.
  Example files can be found in the [regions/](regions/) folder.

  The model weights files must be structured as follows:
  
- A bias term on the first line
- Each subsequent line specifies the weight for a feature. 
- The parameters for each line are separated by a tab character.
- The first parameter is either a + or -, specifying the sign of the second parameter, which is a weight value.
- The third parameter is always a ‘*’ (i.e. multiplication)
- The fourth and final parameter is the feature name

  Example weight files can be found in the [weights/](weights/) folder.

-----------------------

### Project Methods
#### Building a New Model
  The Data.py script handles the extraction of features and building of a dataset. When initializing the Data class, we supply the four annotated region files, specifying the positives and negatives for the training / testing datasets
  
  ```import Data as ds```
  
  ```flowers = ds.Data("positives-train", "negatives-train", "positives-test", "negatives-test")```
  
  To generate a dataset from these files, call the write_to_arff_file method on our data object, specifying the desired prefix for our output files.

  ```flowers.write_to_arff_file("output")```

  Two files are generated, with the suffixes ‘-train.arff’ and ‘-test.arff’ appended, containing the features extracted from the regions for the training and testing datasets respectively.

  From here, we can analyze these datasets using the machine learning tool Weka, to obtain a suitable model for our data.

#### Using the Model to Detect Flower Regions
  Once a suitable model has been created in Weka, we must now create a weights file using the Weka model. Depending on the Weka classifier used, this may require some work to get the file into the correct format.
  
  The svm_eval.py script handles the detection of flower regions within a larger image. Once we have a weight file, we can create an instance of our model.
  
  ```import svm_eval```
  
  ```model = svm_eval.Evaluator('weights')```
  
  To verify the weights file is correct and the model is working correctly, supply the testing set to the confusion_matrix method and ensure the output matches that of the Weka implementation.
  
  ```model.confusion_matrix("positives-test", "negatives-test")```
  
  To test whether the model classifies a specific region as an image, we can call the is_flower method.

  ```model.is_flower(image_region=region)```
  
  This returns True if the model detects a flower, or False if it does not.

  The count_flowers method can be used to count the number of flower regions the classifier detects within the image. As the script is still in the testing phase, this will display the detected regions and wait for user input before continuing. This method takes an image as input, along with various optional parameters to specify the minimum, maximum, and incremental size to consider as regions. Alternately, we may call the count_flowers_from_file method, which takes the path to an image as input rather than an image object.

  ```model.count_flowers_from_file("images/IMG_3791.JPG")```

  Region detection is still a work in progress, and currently does not work very well. To improve results, consider improving the model. This may involve expanding / adjusting the feature set, or expanding / cleaning the raw image dataset. The algorithm for checking regions within a larger image is also still a work in progress, and could be made more efficient. Currently, performance is bottlenecked by the time taken to extract features from a region. This could be improved by offloading to the GPU, and parallelizing to process multiple regions simultaneously.

