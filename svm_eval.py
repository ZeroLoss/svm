from typing import List

import Data as data
import copy
import cv2


# scales an image to the new width, while maintaining the aspect ratio
def downscale_image(img, new_width=600):
    height, width, _ = img.shape
    ratio = new_width / float(width)

    return cv2.resize(img, (new_width, int(ratio * height)))


class Evaluator:
    class ModelFeature:
        def __init__(self, name, weight, sign='+'):
            self.name = name.rstrip("\r\n")
            self.weight = float(weight) * (1 if sign == '+' else -1)

    def __init__(self, model_path):
        self.model = list()
        with open(model_path) as file:
            bias_parts = next(file).split('\t')
            self.bias = float(bias_parts[1]) * (1 if bias_parts[0] == '+' else -1)

            for line in file:
                parts = line.split('\t')
                self.model.append(Evaluator.ModelFeature(parts[3], parts[1], parts[0]))

    # Evaluates the specified image region.
    # parameter img: an image to be evaluated
    # parameter img: a 'Region' object, which specifies a larger image and the part of it to be evaluated
    def evaluate(self, img=None, image_region=None):
        val = self.bias
        if image_region is not None:
            img = cv2.imread(image_region.filename, 1)[image_region.Y: image_region.Y + image_region.height,
                  image_region.X: image_region.X + image_region.width]

        features = data.extract_features(img)
        for param in self.model:
            index = data.Data.feature_names.index(param.name)
            val += features[index] * param.weight

        return val

    def classify(self, img=None, image_region=None):
        return -1 if self.evaluate(img=img, image_region=image_region) < 0 else 1

    def is_flower(self, img=None, image_region=None):
        return self.evaluate(img=img, image_region=image_region) > 0

    def confusion_matrix(self, positives_file, negatives_file):
        TP = 0
        FP = 0
        TN = 0
        FN = 0

        with open(positives_file) as file:
            for line in file:
                parts = line.split()
                for i in range(0, int(parts[1])):
                    prediction = self.classify(image_region=
                                               data.Data.Region(parts[0], parts[4 * i + 2], parts[4 * i + 3],
                                                                parts[4 * i + 4], parts[4 * i + 5]))
                    # print(prediction)
                    if prediction > 0:
                        TP += 1
                    else:
                        FN += 1

        with open(negatives_file) as file:
            for line in file:
                parts = line.split()
                for i in range(0, int(parts[1])):
                    prediction = self.classify(image_region=
                                               data.Data.Region(parts[0], parts[4 * i + 2], parts[4 * i + 3],
                                                                parts[4 * i + 4], parts[4 * i + 5]))
                    # print(prediction)
                    if prediction > 0:
                        FP += 1
                    else:
                        TN += 1

        print("Flower\tNotFlower\t<---(predicted)\n" +
              str(TP) + "\t\t" + str(FN) + "\t\tFlower\n" +
              str(FP) + "\t\t" + str(TN) + "\t\tNotFlower")

    class FlowerRegion:
        def __init__(self, center_x, center_y, radius):
            self.cX = center_x
            self.cY = center_y
            self.r = radius

        def is_near(self, x, y, r):
            radius = max(r, self.r)
            return self.cX - radius <= x <= self.cX + radius and self.cY - radius <= y <= self.cY + radius

    def count_flowers(self, img, min_radius=25, max_radius=50, radius_inc=5):
        image = downscale_image(img, 600)
        height, width, _ = image.shape

        detected_flowers: List[Evaluator.FlowerRegion] = list()

        for radius in range(max_radius, min_radius, -radius_inc):
            for y in range(radius, height - radius, 5):
                for x in range(radius, width - radius, 5):
                    # Ensure this region is not too close to an existing flower
                    if not any(flower.is_near(x, y, radius) for flower in detected_flowers):
                        # Consider regions of varied sizes
                            if self.is_flower(img=image[y - radius: y + radius, x - radius: x + radius]):
                                detected_flowers.append(Evaluator.FlowerRegion(x, y, radius))
                                break  # Do not consider other regions centered at this location (will be overlapping)

        print("Found " + str(len(detected_flowers)) + " flowers")
        for region in detected_flowers:
            cv2.rectangle(image, (region.cX - region.r, region.cY - region.r),
                          (region.cX + region.r, region.cY + region.r), (0, 0, 255))

        cv2.imshow('flowers', image)
        cv2.waitKey(0)
        return len(detected_flowers)

    def count_flowers_from_file(self, filename):
        return self.count_flowers(cv2.imread(filename, 1))
