import arff
import numpy as np
import cv2
import sys
from skimage.feature import hog
from skimage.feature import local_binary_pattern


# Extracts a 3-dimensional vector containing pixel intensities found in the image
# Dimensions are an 8-bin vector corresponding to each of the RGB channels in the image
# Returns a 3-dimensional vector, of shape 8 * 8 * 8
def extract_intensity_hist(image):
    # Normalize the vector (i.e. divide by total number of pixels in image)
    width, height, _ = image.shape
    num_pixels = width * height

    ##########################################################
    # Testing -- Individual colour channels                  #
    ##########################################################
    # hist_r = cv2.calcHist([image], [0], None, [8], [0, 256])
    # hist_g = cv2.calcHist([image], [1], None, [8], [0, 256])
    # hist_b = cv2.calcHist([image], [2], None, [8], [0, 256])
    # return np.append(np.append(hist_r / num_pixels, hist_g / num_pixels), hist_b / num_pixels)

    ##########################################################
    # Testing -- Single, greyscale histogram                 #
    ##########################################################
    # grey_img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # hist_grey = cv2.calcHist([grey_img], [0], None, [8], [0, 256])
    # return hist_grey / num_pixels

    hist = cv2.calcHist([image], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
    return hist / num_pixels


# Extracts Histogram of Gradients (HOG) features
# http://scikit-image.org/docs/dev/api/skimage.feature.html#skimage.feature.hog
def extract_hog(image):
    img = cv2.resize(image, (128, 128))
    hist = hog(img, orientations=9, pixels_per_cell=(32, 32), cells_per_block=(2, 2),
               feature_vector=True, multichannel=True, block_norm='L2-Hys')

    return hist


# Extracts Local Binary Pattern (LPB) features
def extract_lbp(image):
    img = cv2.cvtColor(cv2.resize(image, (48, 48)), cv2.COLOR_BGR2GRAY)
    lbp = local_binary_pattern(img, 24, 8, method="uniform").flatten()

    # print(lbp.shape)
    return lbp


# Finds n corners in the supplied image
# See https://docs.opencv.org/2.4/modules/imgproc/doc/feature_detection.html#goodfeaturestotrack
# Returns a single vector of size 2 * n
def extract_vertices(image, n):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    corners = cv2.goodFeaturesToTrack(gray, n, 0.5, 30)

    #    for item in corners:
    #        x, y = item[0]
    #        cv2.circle(gray, (x, y), 5, 255, -1)
    #
    #    cv2.imshow("Vertice Detection Test", gray)
    #    cv2.waitKey()

    # Normalize the values before returning
    corners_relative = np.zeros(2 * n)
    width, height, _ = image.shape
    for i in range(0, min(len(corners), n)):
        x, y = corners[i].ravel()
        corners_relative[2 * i] = x / width
        corners_relative[2 * i + 1] = y / height

    return corners_relative


# Extract SIFT (Scale Invariant Feature Transform) features from the image
# Number of features may vary, so we discard the least significant features (or pad with 0's if less found)
# https://docs.opencv.org/3.4.2/d5/d3c/classcv_1_1xfeatures2d_1_1SIFT.html
# Returns a vector of shape max_num * 6
def extract_keypoints(image, max_num):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    sift = cv2.xfeatures2d.SIFT_create()

    kp = sift.detect(gray)
    kp = sorted(kp, key=lambda x: x.response, reverse=True)

    width, height, _ = image.shape
    kp = kp[:max_num]
    keypoints = np.zeros((max_num, 6))

    for i in range(0, min(len(kp), max_num)):
        item = kp[i]
        x, y = item.pt
        keypoints[i] = np.array([x / width, y / height, item.size, item.angle, item.response, item.octave])

    #    img = cv2.drawKeypoints(gray, kp, gray, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    #    cv2.imshow("SIFT Detection Test", img)
    #    cv2.waitKey()

    return keypoints


# Extracts a number of features from an image
# Returns a single vector containing features for the image
def extract_features(img):
    intensity_hist = extract_intensity_hist(img).flatten()
    gradient_hist = extract_hog(img)

    return np.concatenate((intensity_hist, gradient_hist))

    # lbp = extract_lbp(img)
    # vertices = extract_vertices(img, 3)
    # keypoints = extract_keypoints(img, 50).flatten()
    # return np.concatenate((intensity_hist, vertices, keypoints))


# Extracts features from a region of a larger image
def extract_features_from_region(img_region):
    return extract_features(cv2.imread(img_region.filename, 1)
                            [img_region.Y: img_region.Y + img_region.height,
                            img_region.X: img_region.X + img_region.width])


# Represents a set of images
# Locates all JPEG files in each of the supplied directories
# positive_directory and negative_directory specify file locations for the labeled images
# Splits positive/negative instances into two sets, with size weighted according to train_test ratio
class Data:
    feature_names = list()
    for i in range(512):
        feature_names.append('colour_feature_' + str(i))
    for i in range(324):
        feature_names.append('hog_feature_' + str(i))
    # for i in range(2304):
    #     feature_names.append("lbp_feature_" + str(i))

    class Region:
        def __init__(self, filename, pos_x, pos_y, width, height):
            self.filename = filename
            self.X = int(pos_x)
            self.Y = int(pos_y)
            self.width = int(width)
            self.height = int(height)

        def get_image(self):
            return cv2.imread(self.filename, 1)[
                  self.Y: self.Y + self.height,
                  self.X: self.X + self.width]

    def __init__(self, train_pos, train_neg, test_pos, test_neg):
        self.train_regions = list()
        self.train_labels = list()
        self.test_regions = list()
        self.test_labels = list()

        with open(train_pos) as file:
            for line in file:
                parts = line.split()
                for i in range(0, int(parts[1])):
                    self.train_regions.append(
                        self.Region(parts[0], parts[4 * i + 2], parts[4 * i + 3], parts[4 * i + 4], parts[4 * i + 5]))
                    self.train_labels.append(1)

        with open(train_neg) as file:
            for line in file:
                parts = line.split()
                for i in range(0, int(parts[1])):
                    self.train_regions.append(
                        self.Region(parts[0], parts[4 * i + 2], parts[4 * i + 3], parts[4 * i + 4], parts[4 * i + 5]))
                    self.train_labels.append(0)

        with open(test_pos) as file:
            for line in file:
                parts = line.split()
                for i in range(0, int(parts[1])):
                    self.test_regions.append(
                        self.Region(parts[0], parts[4 * i + 2], parts[4 * i + 3], parts[4 * i + 4], parts[4 * i + 5]))
                    self.test_labels.append(1)

        with open(test_neg) as file:
            for line in file:
                parts = line.split()
                for i in range(0, int(parts[1])):
                    self.test_regions.append(
                        self.Region(parts[0], parts[4 * i + 2], parts[4 * i + 3], parts[4 * i + 4], parts[4 * i + 5]))
                    self.test_labels.append(0)

        self.train_labels = np.array(self.train_labels).reshape([len(self.train_labels), 1])
        self.test_labels = np.array(self.test_labels).reshape([len(self.test_labels), 1])

    # Iterates over the images in the training set, extracting features from each
    # Returns an array of arrays (features for that image), and an array of labels (0 = negative, 1 = positive)
    def get_training_data(self):
        features = np.empty([len(self.train_regions), len(self.feature_names)])

        print("Obtaining image descriptors for training data...")
        for index in range(len(self.train_regions)):

            features[index] = extract_features(self.train_regions[index].get_image())
            sys.stdout.write("\rProcessed " + str(index) + "/" + str(len(self.train_regions)))
            sys.stdout.flush()

        print("\nFinished processing training data.")
        return features, self.train_labels

    def get_testing_data(self):
        features = np.empty([len(self.test_regions), len(self.feature_names)])

        print("Obtaining image descriptors for testing data...")
        for index in range(len(self.test_regions)):
            features[index] = extract_features(self.test_regions[index].get_image())
            sys.stdout.write("\rProcessed " + str(index + 1) + "/" + str(len(self.test_regions)))
            sys.stdout.flush()

        print("\nFinished processing testing data.")
        return features, self.test_labels

    # Creates an ARFF file containing the information from the dataset.
    # https://www.cs.waikato.ac.nz/~ml/weka/arff.html
    # https://github.com/renatopp/liac-arff
    def write_to_arff_file(self, filename):
        train_feat, train_labels = self.get_training_data()
        test_feat, test_labels = self.get_testing_data()

        attribs = list()
        for index in range(len(self.feature_names)):
            attribs.append((self.feature_names[index], 'NUMERIC'))
        attribs.append(('is_flower', ['1.0', '0.0']))

        train_set = {
            'relation': 'kiwifruit_flowers',
            'attributes': attribs,
            'data': np.append(train_feat, train_labels, axis=1)
        }
        test_set = {
            'relation': 'kiwifruit_flowers',
            'attributes': attribs,
            'data': np.append(test_feat, test_labels, axis=1)
        }

        arff.dump(train_set, open(str(filename + "-train.arff"), "w"))
        arff.dump(test_set, open(str(filename + "-test.arff"), "w"))
